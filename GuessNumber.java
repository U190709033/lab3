import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {
	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number = rand.nextInt(100); //generates a number between 0 and 99
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		int check = 0;
		while (true) {
			int guess = reader.nextInt(); //Read the user input
			if (guess != -1) {
				if (guess == number) {
					System.out.println("\nCongratulations! You won after " + check + " attempts!");
					break;
				} else {
					check += 1;
					System.out.println((guess < number) ? "Sorry!\nMine is greater than your guess." :
							"Sorry!\nMine is less than your guess.");
				}
			} else {
				System.out.println("Sorry, the number was " + number);
				break;
			}
			System.out.print("Type -1 to quit or guess another: ");
		}
		reader.close(); //Close the resource before exiting
	}
}