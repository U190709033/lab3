public class FindPrimes {
    public static void main(String[] args) {
         int your_wish = Integer.parseInt(args[0]);
         FindPrimes(your_wish);
    }
    public static boolean is_Prime(int number) {
        for (int i = 2; i <= number / 2; i++) {
            if (number % i == 0)
                return false;
        }
        return true;
    }
    public static void FindPrimes(int bound) {
        String result = "";
        for (int m = 2; m < bound; m++) {
            if (is_Prime(m) == true)
                result += m + ",";
        }
        System.out.println((result.length()>1) ? result.substring(0, result.length()-1) : result);
    }
}
